/* eslint-disable */
const express = require('express');
const path = require('path');
const history = require('connect-history-api-fallback');
const { sendMail } = require('./mailer.js');

const port = process.env.PORT || 8080;
const directory = path.join(__dirname, 'dist');

let app = express();
let staticFileMiddleware = express.static(directory);

app.use(staticFileMiddleware);
app.use(history());
app.use(staticFileMiddleware);
// needs to be called twice per https://github.com/bripkens/connect-history-api-fallback/tree/master/examples/static-files-and-index-rewrite

app.get('/', (req, res) => {
  res.render(directory + '/index.html');
});

app.get('/mail', async (req, res) => {
  try {
    await sendMail({...req.query})
    res.status(200).send('Success!');
  } catch(error) {
    res.status(400).send(JSON.stringify(error));
  }
})

app.listen(port, () => {
  console.log('Listening on port ' + port);
});
