import Vue from 'vue'
import Router from 'vue-router'
import { firebase } from './firebase'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "dashboard" */ './views/Home.vue')
    },
    {
      path: '*',
      name: 'notfound',
      component: () => import(/* webpackChunkName: "dashboard" */ './views/NotFound.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "auth" */ './views/Login.vue')
    },
    {
      path: '/signup',
      name: 'signup',
      component: () => import(/* webpackChunkName: "auth" */ './views/Signup.vue')
    },
    {
      path: '/forgotpass',
      name: 'forgotpass',
      component: () => import(/* webpackChunkName: "auth" */ './views/ForgotPassword.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import(/* webpackChunkName: "stories" */ './views/Profile.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/profile/:profileId',
      name: 'userProfile',
      component: () => import(/* webpackChunkName: "stories" */ './views/Profile.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      component: () => import(/* webpackChunkName: "stories" */ './views/Users.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/stories',
      name: 'stories',
      component: () => import(/* webpackChunkName: "stories" */ './views/Stories.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/stories/:storyId',
      name: 'story',
      component: () => import(/* webpackChunkName: "stories" */ './views/Stories.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/explore',
      name: 'explore',
      component: () => import(/* webpackChunkName: "stories" */ './views/Explore.vue'),
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) {
    next('/')
  } else {
    next()
  }
})

export const currentRouteContains = (routesArrayToCheck, currentRouteName) => {
  return routesArrayToCheck.indexOf(currentRouteName) > -1
}

export default router
