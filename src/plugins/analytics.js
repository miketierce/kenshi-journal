import Vue from 'vue'
import VueAnalytics from 'vue-analytics'
let started = false

export const initializeAnalytics = (userId, router) => {
  if (!started) {
    started = true
    if (process.env.NODE_ENV === 'production') {
      Vue.use(VueAnalytics, {
        id: 'UA-133843912-1',
        router,
        fields: {
          userId
        },
        autoTracking: {
          exception: true
        }
      })
    } else {
      Vue.use(VueAnalytics, {
        id: 'UA-133843912-2',
        router,
        fields: {
          userId
        },
        autoTracking: {
          exception: true
        }
      })
    }
  }
}
