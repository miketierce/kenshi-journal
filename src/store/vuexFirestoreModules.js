import createEasyFirestore from 'vuex-easy-firestore'
import stories from './modules/stories'
import publicStories from './modules/publicStories'
import entries from './modules/entries'
import user from './modules/user'
import profiles from './modules/profiles'
let vuexFirestoreModules

if (process.env.NODE_ENV === 'localhost' || process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'local') {
  vuexFirestoreModules = createEasyFirestore([
    user,
    entries,
    stories,
    publicStories,
    profiles
  ], { logging: true })
} else {
  vuexFirestoreModules = createEasyFirestore([
    user,
    entries,
    stories,
    publicStories,
    profiles
  ])
}

export async function initializeFirestoreModules (store) {
  await Promise.all([
    store.dispatch('userData/openDBChannel'),
    store.dispatch('storiesData/openDBChannel'),
    store.dispatch('publicStoriesData/openDBChannel'),
    store.dispatch('profilesData/openDBChannel')
  ])
}

export async function closeFirestoreModules (store) {
  await Promise.all([
    store.dispatch('userData/closeDBChannel'),
    store.dispatch('storiesData/closeDBChannel'),
    store.dispatch('publicStoriesData/closeDBChannel'),
    store.dispatch('entriesData/closeDBChannel'),
    store.dispatch('profilesData/closeDBChannel')
  ])
}

export async function clearClientFirestoreData (store) {
  await Promise.all([
    store.dispatch('userData/resetUserState', null, { root: true }),
    store.dispatch('storiesData/resetStoriesState', null, { root: true }),
    store.dispatch('publicStoriesData/resetPublicStoriesState', null, { root: true }),
    store.dispatch('entriesData/resetEntriesState', null, { root: true }),
    store.dispatch('profilesData/resetProfilesState', null, { root: true })
  ])
}

export default vuexFirestoreModules
