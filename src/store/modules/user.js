import { firebase } from '../../firebase'
import { clearClientFirestoreData } from '../vuexFirestoreModules'
import { Notification } from 'element-ui'
import router from '../../router'

const getDefaultState = () => {
  return {
    loggedIn: false,
    authMessage: {},
    user: {}
  }
}

const state = getDefaultState()

const getters = {}

const actions = {
  async signup ({ state, dispatch, commit }, formData) {
    if (state.loggedIn) {
      return
    }
    try {
      await firebase.auth().createUserWithEmailAndPassword(formData.email, formData.password)
      const user = await firebase.auth().currentUser
      const userUpdateWithName = {
        displayName: formData.displayName
      }
      await user.updateProfile(userUpdateWithName)
      await dispatch('updateUser', userUpdateWithName)
      await commit('login', null)
    } catch (error) {
      console.log(error.code)
      console.log(error.message)
    }
  },
  async updateUser ({ dispatch }, userData) { // eslint-disable-line camelcase
    try {
      await dispatch('patch', userData)
    } catch (error) {
      console.log(error)
    }
  },
  async sendResetEmail (store, email) {
    try {
      await firebase.auth().sendPasswordResetEmail(email)
      Notification.success({
        title: 'Success',
        message: 'Password reset email sent. Check your inbox.',
        type: 'success'
      })
    } catch (error) {
      console.log(error)
      Notification.error({
        title: 'Error',
        message: 'Problem sending reset password email! 😢',
        type: 'error'
      })
    }
  },
  async login ({ commit, state }, formData) {
    if (state.loggedIn) {
      return
    }
    try {
      await firebase.auth().signInWithEmailAndPassword(formData.email, formData.password)
      commit('login', null)
      router.push('stories')
      commit('resetAuthMessage', null)
    } catch (error) {
      commit('setAuthMessage', error)
    }
  },
  async logout (store) {
    try {
      router.push('/')
      await firebase.auth().signOut()
      await clearClientFirestoreData(store)
    } catch (error) {
      console.log(error)
    }
  },
  resetAuthMessage ({ commit }) {
    commit('resetAuthMessage', null)
  },
  resetUserState ({ commit }) {
    commit('resetState', null)
  }
}

const mutations = {
  login (state) {
    this._vm.$set(state, 'loggedIn', true)
  },
  logout (state) {
    this._vm.$set(state, 'loggedIn', false)
  },
  setAuthMessage (state, message) {
    this._vm.$set(state, 'authMessage', { ...message })
  },
  resetAuthMessage (state) {
    this._vm.$set(state, 'authMessage', {})
  },
  resetState (state) {
    Object.assign(state, getDefaultState())
  }
}

export default {
  firestorePath: 'users/{userId}',
  firestoreRefType: 'doc',
  moduleName: 'userData',
  statePropName: 'user',
  namespaced: true,
  serverChange: {
    convertTimestamps: {
      created_at: '%convertTimestamp%',
      updated_at: '%convertTimestamp%'
    }
  },
  sync: {
    patchHook: async function (updateStore, doc, store) {
      await updateStore(doc)
      // I don't like this! Using because the modifiedHook for serverChanges doesn't seem
      // to be working...
      setTimeout(async () => {
        await store.dispatch('profilesData/refreshProfiles', null, { root: true })
      }, 1000)
    },
    debounceTimerMs: 0
  },
  state,
  getters,
  actions,
  mutations
}
