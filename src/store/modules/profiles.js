import { arrayUnion, arrayRemove } from 'vuex-easy-firestore'

const getDefaultState = () => {
  return {
    profile: {},
    profiles: []
  }
}

const state = getDefaultState()

const getters = {
  profiles: ({ profiles }) => Object.values(profiles)
}

const actions = {
  async refreshProfiles ({ state, dispatch }) {
    try {
      const id = state.profile.id
      await dispatch('closeDBChannel', { clearModule: true })
      await dispatch('resetProfilesState')
      await dispatch('fetchAndAdd', { orderBy: ['created_at'] })
      await dispatch('setProfile', id)
    } catch (error) {
      console.log(error)
    }
  },
  setProfile ({ state, commit }, profileId) {
    commit('setProfile', state.profiles[profileId])
  },
  async addCoin ({ dispatch }, { authorId, currentUserId }) {
    if (authorId !== currentUserId) {
      await dispatch('patch', {
        id: authorId,
        coins: arrayUnion(currentUserId)
      })
    }
  },
  async removeCoin ({ dispatch }, { authorId, currentUserId }) {
    if (authorId !== currentUserId) {
      await dispatch('patch', {
        id: authorId,
        coins: arrayRemove(currentUserId)
      })
    }
  },
  async resetProfilesState ({ commit, dispatch }) {
    await dispatch('closeDBChannel', { clearModule: true })
    await commit('resetState', null)
  }
}

const mutations = {
  setProfile (state, profile) {
    this._vm.$set(state, 'profile', Object.assign({}, profile))
  },
  resetState (state) {
    const defaultState = getDefaultState()
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
}

export default {
  firestorePath: 'users',
  firestoreRefType: 'collection',
  moduleName: 'profilesData',
  statePropName: 'profiles',
  namespaced: true,
  serverChange: {
    convertTimestamps: {
      created_at: '%convertTimestamp%',
      updated_at: '%convertTimestamp%'
    }
  },
  sync: {
    debounceTimerMs: 0
  },
  state,
  getters,
  actions,
  mutations
}
