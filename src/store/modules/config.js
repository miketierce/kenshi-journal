const getDefaultState = () => {
  return {
    isLoading: false
  }
}

const state = getDefaultState()

const getters = {}

const actions = {
  setLoading ({ commit }, isLoading) {
    commit('setLoading', isLoading)
  },
  async resetEntriesState ({ commit }) {
    await commit('resetState', null)
  }
}

const mutations = {
  setLoading (state, isLoading) {
    this._vm.$set(state, 'isLoading', isLoading)
  },
  resetState (state) {
    const defaultState = getDefaultState()
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
