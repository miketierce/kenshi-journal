import { Notification } from 'element-ui'
import { arrayUnion, arrayRemove } from 'vuex-easy-firestore'

const getDefaultState = () => {
  return {
    stories: {},
    story: {},
    storyEditing: false
  }
}

const state = getDefaultState()

const getters = {
  stories: ({ stories }) => Object.values(stories).sort((first, second) => second.created_at - first.created_at),
  storyById: ({ stories }) => (id) => Object.values(stories).find(story => story.id === id)
}

const actions = {
  async getStoryById (store, storyId) {
    return store.getters.storyById(storyId)
  },
  async getStory ({ dispatch, commit }, { storyId, moduleName }) {
    try {
      const querySnapshot = await dispatch('fetch', { where: [['id', '==', storyId]], limit: 0 })
      let docs = []
      if (!querySnapshot.done) {
        querySnapshot.forEach(doc => docs.push(doc))
        if (docs[0]) {
          await commit('setStory', docs[0].data())
        }
      } else {
        const storyById = await dispatch(`${moduleName}/getStoryById`, storyId, { root: true })
        await commit('setStory', storyById)
      }
    } catch (error) {
      console.log(error)
    }
  },
  changeStoryEditing ({ commit }, isEditing) {
    commit('setStoryEditing', isEditing)
  },
  async updateStory ({ dispatch }, { id, title }) { // eslint-disable-line camelcase
    try {
      await dispatch('patch', { id, title })
    } catch (error) {
      console.log(error)
    }
  },
  async deleteStory ({ dispatch }, { id, profileId }) {
    try {
      await dispatch('delete', id)
      const querySnapshot = await dispatch('entriesData/fetch', { where: [['storyId', '==', id]] }, { root: true })
      let entriesToDelete = []
      querySnapshot.forEach(doc => entriesToDelete.push(doc.id))
      if (entriesToDelete.length > 0) {
        await dispatch('entriesData/deleteBatch', entriesToDelete, { root: true })
        for (let entry of entriesToDelete) {
          dispatch('profilesData/patch', {
            id: profileId,
            stories: arrayRemove(entry)
          }, { root: true })
        }
      }
    } catch (error) {
      console.log(error)
    }
  },
  async createStory ({ dispatch }, { title, privacy, created_by_name, uid }) { // eslint-disable-line camelcase
    try {
      const querySnapshot = await dispatch('fetch', { where: [['title', '==', title]], orderBy: ['created_at'] })
      if (querySnapshot.done !== true) {
        Notification.error({
          title: 'Error',
          message: 'Story by that title already exists.',
          type: 'error'
        })
        return
      }
      const newStoryId = await dispatch('insert', { title, private: privacy, created_by_name, entries: [], likes: [] })

      Notification.success({
        title: 'Success',
        message: 'New story created',
        type: 'success'
      })

      await dispatch('profilesData/patch', {
        id: uid,
        stories: arrayUnion(newStoryId)
      }, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  async resetStoriesState ({ commit, dispatch }) {
    await dispatch('closeDBChannel', { clearModule: true })
    commit('resetState', null)
  }
}

const mutations = {
  setStory (state, story) {
    this._vm.$set(state, 'story', story)
  },
  setStoryEditing (state, isEditing) {
    this._vm.$set(state, 'storyEditing', isEditing)
  },
  resetState (state) {
    const defaultState = getDefaultState()
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
}

export default {
  firestorePath: 'stories',
  firestoreRefType: 'collection',
  moduleName: 'storiesData',
  statePropName: 'stories',
  namespaced: true,
  serverChange: {
    convertTimestamps: {
      created_at: '%convertTimestamp%',
      updated_at: '%convertTimestamp%'
    }
  },
  sync: {
    where: [['created_by', '==', '{userId}']],
    orderBy: ['created_at'],
    debounceTimerMs: 0
  },
  state,
  getters,
  actions,
  mutations
}
