import { Notification } from 'element-ui'
import { arrayUnion, arrayRemove } from 'vuex-easy-firestore'

const getDefaultState = () => {
  return {
    entries: {},
    entry: {
      content: '',
      id: ''
    },
    changeStatus: ''
  }
}

const state = getDefaultState()

const getters = {
  entries: ({ entries }) => Object.values(entries).sort((first, second) => second.created_at - first.created_at),
  combinedContent: ({ entries }, getters) => getters.entries.map(entry => entry.content).reverse().join('')
}

const actions = {
  async saveEntry ({ dispatch }, { storyId, id, content, words, sentences }) { // eslint-disable-line camelcase
    await dispatch('patch', {
      id,
      content,
      words,
      sentences
    })
    if (id && id.length > 0) {
      await dispatch('storiesData/patch', {
        id: storyId,
        entries: arrayUnion(id)
      }, { root: true })
    }
  },
  async deleteEntry ({ dispatch }, { id, storyId, profileId }) { // eslint-disable-line camelcase
    try {
      await dispatch('delete', id)
      await dispatch('storiesData/patch', {
        id: storyId,
        entries: arrayRemove(id)
      }, { root: true })
      await dispatch('profilesData/patch', {
        id: profileId,
        entries: arrayRemove(id)
      }, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  setEntry ({ commit, state }, entryId) {
    commit('setEntry', state.entries[entryId])
  },
  async createEntry ({ dispatch }, { storyId, content, profileId }) { // eslint-disable-line camelcase
    try {
      const newEntryId = await dispatch('insert', { storyId, content })
      Notification.success({
        title: 'Success',
        message: 'New entry created',
        type: 'success'
      })
      await dispatch('profilesData/patch', {
        id: profileId,
        entries: arrayUnion(newEntryId)
      }, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  async getEntries ({ dispatch }, storyId) { // eslint-disable-line camelcase
    try {
      await dispatch('fetchAndAdd', { where: [['storyId', '==', storyId]], orderBy: ['created_at'], limit: 50 })
    } catch (error) {
      console.log(error)
    }
  },
  setChangeStatus ({ commit }, status) {
    commit('setChangeStatus', status)
  },
  async resetEntriesState ({ commit, dispatch }) {
    await dispatch('closeDBChannel', { clearModule: true })
    await commit('resetState', null)
  }
}

const mutations = {
  setEntry (state, entry) {
    this._vm.$set(state, 'entry', Object.assign({}, entry))
  },
  setChangeStatus (state, status) {
    this._vm.$set(state, 'changeStatus', status)
  },
  resetState (state) {
    const defaultState = getDefaultState()
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
}

export default {
  firestorePath: 'entries',
  firestoreRefType: 'collection',
  moduleName: 'entriesData',
  statePropName: 'entries',
  namespaced: true,
  serverChange: {
    convertTimestamps: {
      created_at: '%convertTimestamp%',
      updated_at: '%convertTimestamp%'
    }
  },
  sync: {
    orderBy: ['created_at'],
    debounceTimerMs: 0
  },
  state,
  getters,
  actions,
  mutations
}
