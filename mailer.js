const { struct } = require('superstruct')
const nodemailer = require('nodemailer')

const MailOptionType = struct({
  from: 'string',
  to: 'string',
  subject: 'string',
  html: 'string'
}, { from: 'kenshijournal@gmail.com' })

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'kenshijournal@gmail.com',
    pass: process.env.EMAIL_PASSWORD
  }
})

module.exports = {
  sendMail: async (options) => {
    try {
      await transporter.sendMail(MailOptionType(options))
    } catch (error) {
      console.log('Error sending email with options:')
      console.log(options)
    }
  }
}
