<div align="center"><a href="https://www.rolefiction.com" target="_blank" rel="noopener noreferrer"><img width="200" height="200" src="https://gitlab.com/bpkennedy/kenshi-journal/raw/master/src/assets/roleFictionLogo.svg" alt="RoleFiction logo"></a></div>
RoleFiction is a social roleplaying fiction writing platform for video game communities. Use it to enhance your own role playing narrative. Share your stories with your friends!


| Staging  | Production  |
|---|---|
| [![staging](https://gitlab.com/bpkennedy/kenshi-journal/badges/staging/pipeline.svg?style=flat-square)](https://gitlab.com/bpkennedy/kenshi-journal/commits/staging) | [![production](https://gitlab.com/bpkennedy/kenshi-journal/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/bpkennedy/kenshi-journal/commits/master) |

<a href="http://www.browserstack.com" target="_blank"><img src="https://gitlab.com/bpkennedy/kenshi-journal/raw/master/A3D17D5B-CAED-4567-AC71-A4D1BC5E86BE.png" width="280" height="92" alt="Browserstack Logo"/></a>

For developers
============

## Prerequisites
There is an .env.development.local file with the sensitive connection info to the staging Google Cloud firestore database (used during development and testing).  You will need a copy of this to run locally. 

### NPM scripts
* Install: `npm install`
* Serve: `npm run serve`
* Build for Staging: `npm run build:staging`
* Build for Production: `npm run build`
* Lint: `npm run lint`
* Electron serve: `npm run electron:serve`
* Electron build: `npm run electron:build`
