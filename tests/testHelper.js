import { Selector, t } from 'testcafe'

const navStories = Selector('*[aria-label="Stories Nav Item"]')
const navExplore = Selector('*[aria-label="Explore Nav Item"]')

const loginButton = Selector('*[aria-label="Login Button"]')
const loginAction = Selector('*[aria-label="Login"]')
const signUpLink = Selector('*[aria-label="Signup Link"]')
const createStoryButton = Selector('*[aria-label="Create Story Button"]')
const createStoryModalButton = Selector('*[aria-label="Create Story Modal Button"]')
const createEntryButton = Selector('*[aria-label="Create Entry Button"]')
const userDropdown = Selector('*[aria-label="User Photo Dropdown Menu"]')
const logoutMenuItem = Selector('*[aria-label="Logout Menu Item"]')
const profileMenuItem = Selector('*[aria-label="Profile Menu Item"]')
const emailInput = Selector('#email')
const passwordInput = Selector('#password')

const themeInput = Selector('#theme')
const themeElementMenuItem = Selector('*[aria-label="Element Menu Item"]')
const themeKenshiMenuItem = Selector('*[aria-label="Kenshi Menu Item"]')

const signupEmail = Selector('*[aria-label="Signup Email"]')
const signupDisplayname = Selector('*[aria-label="Signup Display Name"]')
const signupPassword = Selector('*[aria-label="Signup Password"]')
const signupConfirmPassword = Selector('*[aria-label="Signup Confirm Password"]')
const signupButton = Selector('*[aria-label="Signup Button"]')

const storyNameField = Selector('*[aria-label="Story Name Field"]')
const storyPrivacyField = Selector('*[aria-label="Story Privacy Field"]')
const privacyFieldPrivate = Selector('*[aria-label="Privacy Field Private"]')
const privacyFieldPublic = Selector('*[aria-label="Privacy Field Public"]')
const textEditorArea = Selector('*[aria-label="Text Editor"]').find('div.ql-editor')

const notificationCloseButton = Selector('.el-notification__closeBtn')

export function clearCache () {
  indexedDB.deleteDatabase('firebaseLocalStorageDb')
}

export async function signIn (username, password) {
  await t.hover(loginButton)
    .click(loginButton)
    .click(emailInput)
    .typeText(emailInput, username)
  await t.click(passwordInput)
    .typeText(passwordInput, password)
  await t.click(loginAction)
    .expect(userDropdown.visible)
    .ok()
}

export async function signOut () {
  await t.hover(userDropdown)
    .click(userDropdown)
    .hover(logoutMenuItem)
    .click(logoutMenuItem)
    .expect(loginButton.visible)
    .ok()
}

export async function createAccount (email, displayName, password, confirmPassword) {
  await t.hover(signUpLink)
    .click(signUpLink)
    .click(signupEmail)
    .typeText(signupEmail, email)
  await t.click(signupDisplayname)
    .typeText(signupDisplayname, displayName)
  await t.click(signupPassword)
    .typeText(signupPassword, password)
  await t.click(signupConfirmPassword)
    .typeText(signupConfirmPassword, confirmPassword)
  await t.hover(signupButton)
    .click(signupButton)
    .expect(userDropdown.visible)
    .ok()
}

export async function goToUserProfile () {
  await t.expect(userDropdown.visible)
    .ok()
    .hover(userDropdown)
    .click(userDropdown)
    .hover(profileMenuItem)
    .click(profileMenuItem)
}

export async function goToUserStories () {
  await t.expect(navStories.visible)
    .ok()
    .hover(navStories)
    .click(navStories)
}

export async function goToExplore () {
  await t.expect(navExplore.visible)
    .ok()
    .hover(navExplore)
    .click(navExplore)
}

export async function createStory (storyTitle, privacy) {
  await t.hover(createStoryButton)
    .click(createStoryButton)
    .click(storyNameField)
    .typeText(storyNameField, storyTitle)
  await t.click(storyPrivacyField)
  if (privacy === 'Private') {
    await t.click(privacyFieldPrivate)
  } else {
    await t.click(privacyFieldPublic)
  }
  await t.hover(createStoryModalButton)
    .click(createStoryModalButton)
    .hover(notificationCloseButton)
    .click(notificationCloseButton)
    .expect(Selector(`*[aria-label="Story Title ${storyTitle}"]`).visible)
    .ok()
}

export async function createEntry (storyTitle, entryText) {
  const story = Selector(`*[aria-label="Story Title ${storyTitle}"]`)
  await t.expect(story)
    .ok()
  await t.hover(story)
    .click(story)
    .expect(storyTitle)
    .contains(storyTitle)
  await t.hover(createEntryButton)
    .click(createEntryButton)
    .hover(notificationCloseButton)
    .click(notificationCloseButton)
    .hover(Selector('*[aria-label="Entry List Item"]'))
    .click(Selector('*[aria-label="Entry List Item"]'))
    .hover(textEditorArea)
    .click(textEditorArea)
    .typeText(textEditorArea, entryText)
  await t.expect(textEditorArea.textContent)
    .contains(entryText)
  await t.expect(Selector('*[aria-label="Entry Title"]').innerText)
    .contains(entryText)
}

export async function verifyStoryIsPresent (storyTitle) {
  await t.expect(Selector(`*[aria-label="Story Title ${storyTitle}"]`).exists).ok()
}

export async function verifyStoryNotPresent (storyTitle) {
  await t.expect(Selector(`*[aria-label="Story Title ${storyTitle}"]`).exists).notOk()
}

export async function changeTheme (themeName) {
  await t.click(themeInput)
  if (themeName === 'Kenshi') {
    await t.click(themeKenshiMenuItem)
  } else {
    await t.click(themeElementMenuItem)
  }
}

export async function likeStory (storyTitle) {
  const likeButton = Selector(`*[aria-label="Like ${storyTitle}"]`)
  await t.hover(likeButton)
    .click(likeButton)
    .expect(Selector('*[aria-label="Like Count"]').innerText).eql('1')
}
