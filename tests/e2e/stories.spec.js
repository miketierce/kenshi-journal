import { signIn, signOut, createAccount, goToUserStories, likeStory, goToExplore, createStory, createEntry, verifyStoryIsPresent, verifyStoryNotPresent } from '../testHelper.js'
import { setup, teardown } from '../hooks.js'

fixture`Stories`.page('http://localhost:8080').after(teardown) /* eslint-disable-line no-undef */

test('Can create a public story with one entry', async t => { /* eslint-disable-line no-undef */
  await signIn('bpkennedy@gmail.com', 'pass123')

  // make a story
  await goToUserStories()
  await createStory('TestStoryTitle1', 'Public')

  // should not show in Public because there are 0 entries
  await goToExplore()
  await verifyStoryNotPresent('TestStoryTitle1')

  // make an entry
  await goToUserStories()
  await createEntry('TestStoryTitle1', 'sometesttext')

  // should show in Stories route
  await goToUserStories()
  await verifyStoryIsPresent('TestStoryTitle1')

  // should now show in Explore route
  await goToExplore()
  await verifyStoryIsPresent('TestStoryTitle1')
}).before(setup).after(teardown)

test('Can create a private story with one entry', async t => { /* eslint-disable-line no-undef */
  await signIn('bpkennedy@gmail.com', 'pass123')

  // make a story
  await goToUserStories()
  await createStory('TestStoryTitle1', 'Private')

  // should NEVER show in Explore route because this is private story
  await goToExplore()
  await verifyStoryNotPresent('TestStoryTitle1')

  // make an entry
  await goToUserStories()
  await createEntry('TestStoryTitle1', 'sometesttext')

  // should show in Stories route
  await goToUserStories()
  await verifyStoryIsPresent('TestStoryTitle1')

  // should NEVER show in Explore route because this is private story
  await goToExplore()
  await verifyStoryNotPresent('TestStoryTitle1')
}).before(setup).after(teardown)

test('Can like another person\'s Public story', async t => { /* eslint-disable-line no-undef */
  await signIn('bpkennedy@gmail.com', 'pass123')

  // make a public story with entry
  await goToUserStories()
  await createStory('TestStoryTitle1', 'Public')
  await createEntry('TestStoryTitle1', 'sometesttext')

  // sign out and create new account
  await signOut()
  await createAccount('test@email.com', 'TestGuy', 'pass123', 'pass123')

  // go to Explore route and like story
  await goToExplore()
  await likeStory('TestStoryTitle1')
}).before(setup).after(teardown)

test('Can only see your own private stories but any public story', async t => { /* eslint-disable-line no-undef */
  await signIn('bpkennedy@gmail.com', 'pass123')

  // make a public story with entry
  await goToUserStories()
  await createStory('TestStoryTitle1', 'Public')
  await createEntry('TestStoryTitle1', 'sometesttext')
  await goToUserStories()
  await verifyStoryIsPresent('TestStoryTitle1')
  await goToExplore()
  await verifyStoryIsPresent('TestStoryTitle1')

  // make a private story with entry
  await goToUserStories()
  await createStory('TestStoryTitle2', 'Private')
  await createEntry('TestStoryTitle2', 'someothertesttext')
  await goToUserStories()
  await verifyStoryIsPresent('TestStoryTitle2')
  await goToExplore()
  await verifyStoryNotPresent('TestStoryTitle2')

  // sign out and create new account
  await signOut()
  await createAccount('test@email.com', 'TestGuy', 'pass123', 'pass123')

  // go to Explore route and should see only one story
  await goToExplore()
  await verifyStoryIsPresent('TestStoryTitle1')
  await verifyStoryNotPresent('TestStoryTitle2')

  // go to Stories and should see no stories (because one is private of another user)
  await goToUserStories()
  await verifyStoryNotPresent('TestStoryTitle1')
  await verifyStoryNotPresent('TestStoryTitle2')
}).before(setup).after(teardown)
