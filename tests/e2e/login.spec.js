import { signIn, signOut, createAccount } from '../testHelper.js'
import { setup, teardown } from '../hooks.js'

fixture`SignIn/SignUp`.page('http://localhost:8080').after(teardown) /* eslint-disable-line no-undef */

test('Can sign in', async t => { /* eslint-disable-line no-undef */
  await signIn('bpkennedy@gmail.com', 'pass123')
}).before(setup).after(teardown)

test('Can sign out', async t => { /* eslint-disable-line no-undef */
  await signIn('bpkennedy@gmail.com', 'pass123')
  await signOut()
}).before(setup).after(teardown)

test('Can create a new account', async t => { /* eslint-disable-line no-undef */
  await createAccount('test@email.com', 'TestGuy', 'pass123', 'pass123')
}).before(setup).after(teardown)
