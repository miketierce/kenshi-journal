import { Selector } from 'testcafe'

import { signIn, goToUserProfile, changeTheme, signOut } from '../testHelper.js'
import { setup, teardown } from '../hooks.js'

fixture`User settings`.page('http://localhost:8080').after(teardown) /* eslint-disable-line no-undef */

test('Can change theme from user profile, remembers', async t => { /* eslint-disable-line no-undef */
  await signIn('bpkennedy@gmail.com', 'pass123')
  await goToUserProfile()
  await changeTheme('Kenshi')
  await t.expect(Selector('.kenshi').count).eql(1)
  await signOut()
  await t.expect(Selector('.kenshi').count).eql(0)
  await signIn('bpkennedy@gmail.com', 'pass123')
  await t.expect(Selector('.kenshi').count).eql(1)
}).before(setup).after(teardown)
